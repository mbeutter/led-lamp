#include <JeeLib.h>

MilliTimer sendTimer;

uint8_t lamp1Level, lamp2Level;

uint8_t myID=21;

typedef struct {
    uint8_t id;
    uint8_t lamp;
    uint8_t level;
} Payload;

Payload inData, outData;
byte pendingOutput, action;
byte numberSet, levelSet;

const int log32[32]= {0, 1, 2, 2, 2, 3, 3, 4, 5, 6, 7, 8, 10, 11,
                  13, 16, 19, 23, 27, 32, 38, 45, 54, 64, 76,
                  91, 108, 128, 152, 181, 215, 255};
// array from http://www.mikrocontroller.net/articles/LED-Fading

const int log25[25]={0,1,2,2,3,3,4,5,6,8,10,13,16,20,25,32,
                    40,51,64,80,101,128,161,202,255};
                    
// boilerplate for low-power waiting
//ISR(WDT_vect) { Sleepy::watchdogEvent(); }

extern InputParser::Commands cmdTab[] PROGMEM;
InputParser parser (50, cmdTab);

static void levelCmd () {
    //byte v;
    parser >> outData.level;
    //outData.level = v;
    Serial.print("level = ");
    Serial.print(outData.level);
    Serial.print("\n");
    levelSet=1;
}

static void numberCmd () {
    byte v;
    parser >> v;
    outData.lamp = v;
    Serial.print("number = ");
    Serial.print(outData.lamp);
    Serial.print("\n");
    numberSet=1;
}

InputParser::Commands cmdTab[] = {
    { 'l', 1, levelCmd },
    { 'n', 1, numberCmd },
    { 0 }    
};


static void consumeInData() {
}

static byte produceOutData() {

    levelSet=0;
    numberSet=0; 
    Serial.print("OutData produced!\n");
    return 1;       
}

void setup() {
    rf12_initialize(myID, RF12_868MHZ);
    outData.id=10;
    numberSet=0;
    levelSet=0;
    pendingOutput=0;
    Serial.begin(57600);
    Serial.print("Setup done\n");
//    delay(4100);
}

void loop() {
     if (rf12_recvDone() && rf12_crc == 0 && rf12_len == sizeof inData) {
        memcpy(&inData, (byte*)  rf12_data, sizeof inData);
        consumeInData();
    }       
   if (sendTimer.poll(100) && levelSet && numberSet)
        pendingOutput = produceOutData();
        
    if (pendingOutput && rf12_canSend()) {
        rf12_sendStart(0, &outData, sizeof outData, 2);
        rf12_sendWait(2);
        Serial.print("Data sent\n");
        pendingOutput=0;
    }

    if (!pendingOutput)
        parser.poll();
}
