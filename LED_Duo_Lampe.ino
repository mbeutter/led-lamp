#include <JeeLib.h>

RemoteNode::Data state;
Port lamp1 (2);
Port lamp2 (3);

uint8_t lamp1Level, lamp2Level;

uint8_t myID=10;

typedef struct {
    uint8_t id;
    uint8_t lamp;
    uint8_t level;
} Payload;

Payload inData;

const int log32[32]= {0, 1, 2, 2, 2, 3, 3, 4, 5, 6, 7, 8, 10, 11,
                  13, 16, 19, 23, 27, 32, 38, 45, 54, 64, 76,
                  91, 108, 128, 152, 181, 215, 255};
// array from http://www.mikrocontroller.net/articles/LED-Fading

const int log25[25]={0,1,2,2,3,3,4,5,6,8,10,13,16,20,25,32,
                    40,51,64,80,101,128,161,202,255};
                    
const int shortFade = 1000;
const int longFade = 3000;

// boilerplate for low-power waiting
//ISR(WDT_vect) { Sleepy::watchdogEvent(); }

void fade(Port* outPort, int time, int startVal, int endVal) {
    int endIndex = endVal/4;
    int startIndex = startVal/4;
    int up, steps;
    int timeStep;

    if (endIndex > 24)
        endIndex = 24;
    if (startIndex > 24)
        startIndex = 24;
    if (endVal > startVal) {
        steps = endIndex - startIndex;
        timeStep = time/steps;
        for (int i=startIndex; i<=endIndex; i++) {
            outPort->anaWrite(log25[i]);
            delay(timeStep);
        }
    }
    else {
        steps = startIndex - endIndex;
        timeStep = time/steps;
        for (int i=startIndex; i>=endIndex; i--) {
            outPort->anaWrite(log25[i]);
            delay(timeStep);
        }
    }
}

void fadeIn(Port* outPort, int time)
{
    fade(outPort, time, 0, 100);
}

void fadeOut(Port* outPort, int time)
{
    fade(outPort, time, 100, 0);
}
     
static void consumeInData() {
    Port* lampNo;
    uint8_t level;
    
    Serial.print("consumeInData\n");
    Serial.print("id = ");
    Serial.print(inData.id");
    Serial.print(" -*- Lamp No. = ");
    Serial.print(inData.lamp);
    Serial.print(" -*- Level = ");
    Serial.println(inData.level);
    if (inData.id == myID) {
        Serial.print("Lampe No. ");
        Serial.println(inData.lamp);
        switch (inData.lamp) {
            case 1:
                lampNo = &lamp1;
                level = lamp1Level;
                lamp1Level = inData.level;
                break;
            case 2:
                lampNo = &lamp2;
                level = lamp2Level;
                lamp2Level = inData.level;
                break;
        }
        Serial.print("Old level = ");
        Serial.print(level);
        Serial.print("   New level = ");
        Serial.println(inData.level);
        fade(lampNo, shortFade, level, inData.level);
        if (inData.level == 0) 
            lampNo->digiWrite(0);
    }  
                
}    

void setup() {
    rf12_initialize(myID, RF12_868MHZ);
    lamp1.mode(OUTPUT);
    lamp2.mode(OUTPUT);
    lamp1Level = 0;
    lamp2Level = 0;
    Serial.begin(57600);
    Serial.print("Setup done\n");
}

void loop() {
     if (rf12_recvDone() && rf12_crc == 0 && rf12_len == sizeof inData) {
        memcpy(&inData, (byte*)  rf12_data, sizeof inData);
        consumeInData();
    }                   
}

